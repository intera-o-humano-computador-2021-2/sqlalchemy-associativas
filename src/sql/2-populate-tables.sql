USE db_work;

INSERT INTO tb_servico(dsc_servico, vlr_servico, tmp_servico)
VALUES
    ('Lavagem a seco de Sofá', 149.00, 1),
    ('Desentupimento de pias', 39.00, 1),
    ('Desentupimento de calhas', 99.00, 1),
    ('Dedetização de casa', 199.00, 2),
    ('Lavagem de pinturas externas', 299.00, 4);



INSERT INTO tb_cliente(nme_cliente, end_cliente, tel_cliente)
VALUES
    ('João Macário', 'QNA-26 Casa-4', '98764567');

INSERT INTO ta_agendamento(cod_servico, cod_cliente, dti_agendamento)
VALUES
    (1, 1, '2021-10-09 14:00'),
    (3, 1, '2021-10-11 08:00'),
    (5, 1, '2021-10-21 09:00');



INSERT INTO tb_cliente(nme_cliente, end_cliente, tel_cliente)
VALUES
    ('Naara Pereira', 'QI-24 F 18', '92787654');

INSERT INTO ta_agendamento (cod_servico, cod_cliente, dti_agendamento)
VALUES
    (1, 2, '2021-11-10 10:00'),
    (2, 2, '2021-10-15 09:00'),
    (4, 2, '2021-12-02 15:00');
