from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.automap import automap_base
from sqlalchemy.sql import func

from datetime import datetime

# VARIÁVEIS GLOBAIS ------------------------------------
# Ligação com o esquema de banco de dados
engine = create_engine("mysql+mysqlconnector://root:hiragi7@db/db_work?charset=utf8mb4")

# Mapeamento Objeto Relacional com o SQLAlchemy
DB = automap_base()
DB.prepare(engine, reflect=True)
tb_servico = DB.classes.tb_servico
tb_cliente = DB.classes.tb_cliente
ta_agendamento = DB.classes.ta_agendamento

# Trabalho com sessões da base agora Objeto-Relacional
session_factory = sessionmaker(bind=engine)
ses = session_factory()
#-------------------------------------------------------

continuar = True
while continuar:
   print("\n" * 30)
   # Descobrindo o menor e maior identificador cadastrado
   menor, maior = ses.query(func.min(tb_cliente.idt_cliente), func.max(tb_cliente.idt_cliente)).first()
   idt = int(input('Agendar serviço para o cliente? [{}-{}]: '.format(menor, maior)))
   cli = ses.query(tb_cliente).filter_by(idt_cliente=idt).first()

   print("\n" * 30)
   # Listar os agendamentos cadastrados
   agendamentos = ses.query(ta_agendamento).filter_by(cod_cliente=idt).all()
   servicos = ses.query(tb_servico).all()

   def get_servico(idt_servico):
     for s in servicos:
       if (s.idt_servico == idt_servico):
         return s

   print('Agendamentos:')
   print('-' * 40)
   for a in agendamentos:
       print(a.idt_agendamento, '-', get_servico(a.cod_servico).dsc_servico, a.dti_agendamento.strftime('%Y-%m-%d %H:%M'))
   print('-' * 40)
   print('Cliente:', cli.nme_cliente)
   idt = int(input('Qual número do agendamento para cancelar? '))

   serv = ses.query(ta_agendamento).filter_by(cod_servico=idt).delete()
   ses.commit()

   # Listar os agendamentos do cliente
   print("\n" * 30)
   print('Cliente:', cli.nme_cliente)
   print('-' * 40)
   for a in cli.ta_agendamento_collection:
       print(a.tb_servico.dsc_servico, a.dti_agendamento)

   opc = input('Cancelar mais um agendamento [S-N]? ')
   if opc.upper() == 'N':
       continuar = False

ses.close()
